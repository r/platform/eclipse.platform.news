<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Language" content="en-us" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="default_style.css" type="text/css" title="main" media="screen" />
<title>Orion 5.0 News</title>
</head>
<body>

<h3 id = "new-look">New Orion look and feel</h3>
New Orion users have consistently reported that they found our cluster of iconic <b>Actions</b> and <b>Navigation</b> buttons confusing. Developers often have trouble finding the commands they are familiar with in their old desktop-based tools. So, finally we are caving to user demand and incorporating some familiar navigation elements such as a menu bar and context menus. You can find more details on the various UI changes further down in this post. User feedback has been a tremendously important part of these changes, and we strongly encourage all users to continue providing this feedback as the design evolves. Please don't hesitate to raise bugs, report feedback on the orion mailing list, or grab us in person at conferences such as EclipseCon.

<h3 id="nav-bar">New navigation bar</h3>
The old <b>Navigation</b> menu has been replaced by a narrow band on the left hand side with links to all of Orion's different pages. All links are contextual and will navigate you to the closest corresponding page related to the artifact you are currently viewing. For example when editing a file, navigating to <b>Git Status</b> will show Git status for the repository containing the file you were just editing. Icons in the navigation bar match the page icon in your browser tab, to help you keep things straight. You can collapse the navigation bar at any time by clicking the toggle button in the top left hand corner of each page.
<p><img src="images/nav-bar.png" alt="Navigation bar"/></p>

<h3 id="menu-bar">Menu bar</h3>
The <b>Actions</b> menu, and some navigation elements, have been replaced with a more traditional menu bar. The key binding shortcuts have been added to the menu label for all commands that have them. The options to toggle between navigation and outline panels have been moved to the <b>View</b> menu.

<p><img src="images/menu-bar.png" alt="Menu bar"/></p>

<h3 id="context-menu">Context menu in Navigation pane</h3>
Context menus have been added on files and folders in the <b>Navigation</b> pane. In these menus you will find the most common file operations such as copy, paste, rename, compare, etc. You will also find commands for navigating to other pages related to that file or folder.

<p><img src="images/context-menu.png" alt="Context menu"/></p>

<h3 id="content-assist">Content assist enhancements</h3>
The Orion content assist infrastructure has had a major overhaul. Orion content assist is now:
<ul>
	<li><b>Faster</b>. Computing, loading, and rendering of content assist proposals has been made much snappier.</li>
	<li><b>More stylish</b>. The proposals have been styled, so that the value to be inserted is highlighted, and peripheral information such as type information and descriptions are less distinct. Also, template proposals have been moved to a different section with a colored background to separate them from completions inferred from the current context. Highlighting of the current proposal has been improved, and scrollbars now only show when needed.</li>
	<li><b>Easier to use</b>. Content assist will now trigger automatically in the places where you most expect it to, such as after typing '.' in JavaScript or opening a tag in HTML. Also, if you invoke content assist and there is only one possible match, it will be inserted automatically. Finally, the Home/End keys can now be used to quickly navigate around the proposal list.</li>
</ul>
To give you a feel for the overall changes, here is a comparison of content assist in Orion 5.0, versus the previous release:

<p><img src="images/50m2-content-assist.png" alt="Improved content assist"/></p>

<h3 id="node-assist">Content assist for popular Node.js libraries</h3>
Orion has added indexes to support content assist for some of the most popular Node.js libraries. On the database front there is now content assist for Redis, MySQL, Postgres, and MongoDB. Content assist is now also available for the AMQP messaging library, and the Express.js framework. Templates for popular code snippets with these various libraries are also available. Orion plugins can now contribute indexes for other libraries using the <tt>orion.core.typedef</tt> service.

<h3 id="eslint">Fully configurable JavaScript syntax validation</h3>
Orion is now almost fully converted over to the fantastic <a href="https://github.com/eslint/eslint">eslint</a> library for JavaScript syntax validation. The only exception is script tags in HTML files, which are still validated with jslint for now. A huge advantage of eslint is its configurability. As a result, we now expose a large selection of lint configuration options on the <b>Validation</b> settings page.

<p><img src="images/50m2-validation-settings.png" alt="Validation settings page"/></p>

<h3 id="json-lint">Syntax validation of JSON</h3>
Orion now provides syntax validation for <a href="http://json.org/">JSON</a> files.

<p><img src="images/50m2-json-lint.png" alt="JSON syntax highlighting"/></p>

<h3 id="gerrit">Improved Gerrit support</h3>
Orion is steadily making progress towards support for <a href="https://code.google.com/p/gerrit/">Gerrit</a> workflows. In the current release the following features have been added to make it easier to use Orion with Gerrit:
<p>
Support to attach a Gerrit change-id to your commits:
</p>
<p><img src="images/50m2-gerrit-changeid.png" alt="Gerrit change id in commit dialog"/></p>
<p>
Git push without tags. When pushing a change to Gerrit for review you don't want to push tags as well. The <b>Push</b> button now has a drop-down menu on the right that provides a <b>Push Branch</b> option to push commits only.
</p>
<p><img src="images/50m2-gerrit-push-branch.png" alt="Push branch without tags"/></p>
<p>
Push for review. When a contributor wants to submit a proposed patch for review in Gerrit, they push to a special branch with the format <tt>refs/for/[branchName]</tt>. Previously Orion always assumed pushes occurred to <tt>heads</tt> branches. Now, when you select <b>Push</b> followed by <b>More</b>, you can enter a branch name such as <tt>refs/for/master</tt> to push a change for review that is intended for the <b>master</b> branch.
</p>
<p><img src="images/50m2-push-review.png" alt="Git push to Gerrit for review"/></p>

<h3 id="syntax-highlight">New pluggable syntax highlighting system</h3>
The Orion editor has adopted a new syntax highlighting framework, designed specifically for Orion. Notable improvements include better performance, and built in support for more languages (currently JavaScript, JSON, XML, CSS, HTML, Java, PHP, Python, Ruby, and YAML). Syntax highlighting for additional languages can be provided by plugins. In addition, the syntax highlighting framework can be used with the <a href="http://git.eclipse.org/c/orion/org.eclipse.orion.client.git/tree/bundles/org.eclipse.orion.client.editor/web/examples/editor/edit.html?id=e06ad033cf09495b71bef4f0255d397ede2b2971">standalone Orion editor</a>, which is handy if you want a smaller package for embedding in your web application. 

For more details on the new syntax highlighting framework, checkout this recent <a href="http://planetorion.org/news/2014/02/orion-5-0-syntax-styling-revisited/">blog post</a> on Planet Orion.

<h3 id="filter-folder-in-find">Filter by folder when searching for files</h3>
When using the Orion <b>Find File</b> command, you can now filter the results list based on keywords in the directory path by specifying a second search term in the find box. This is very handy when there are a large number of duplicates of the same file name and you want to quickly narrow down to a particular match.

<p><img src="images/50m2-find-file-filter.png" alt="Filtering by folder in find file"/></p>

<h3 id="edit-compare">Edit while comparing files</h3>
When you select two files and pick <b>Compare with each other</b> from the context menu, the resulting compare page now allows you to edit both files directly within the compare editor. Unsaved changes are indicated by an asterisk next to the name, and <b>Save</b> buttons appear above each editor. Saving changes on either side will cause the comparisons to be updated immediately.

<p><img src="images/50m2-edit-compare.png" alt="Edit in compare page"/></p>

That's all for the current milestone! Make sure you also check out the <a href="http://planetorion.org/news/2013/12/orion-5-0-m1-new-and-noteworthy/">5.0 M1</a> news for other features added earlier in the 5.0 release.

</body>
</html>
